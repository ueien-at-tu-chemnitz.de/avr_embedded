# AVR Chip Embedded Lab
> author: Boyue, Zeng

#### Timeline
| Task  | Start Data  | Finish Date  | Actually Date  |   
|---|---|---|---|
| Task1  | 07.04  |   |   |
| Task2  |   |   |   |
| Task3  |   |   |   |

#### Reference
1. [Arduino Pin Mapping](https://docs.arduino.cc/retired/hacking/hardware/PinMapping168/)