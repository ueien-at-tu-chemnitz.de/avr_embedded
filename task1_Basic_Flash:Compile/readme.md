# Task 1 Basic and Compile/Flash

The task is easy. You need to understand how the code is compiled and flashed into the microcontroller. Plus, you should also know the details of compiling process and its relating hardware. Although tasks are most done by Arduino IDE, you should be able use/understand the compiler or programmer which are used in Arduino IDE. 

This task has two sections, the first one is the ***practice section*** and the second one is ***theory question***. In **theory question**, you should be able to answer the questions and present the questions by any kind of forms, etc figures or table (whatevery you want), as long as you can explain the answers. (PS: remember to include your references.)


### 1. [Practice] Flash the following code

> The purpose of this sub-task is to let you understand how to compile and flash and also make sure you configura the right development environment for the future tasks.

```
/* Blinker Demo */
// ------- Preamble -------- //
#include <avr/io.h>                        /* Defines pins, ports, etc */
#include <util/delay.h>                     /* Functions to waste time */


int main(void) {

  // -------- Inits --------- //
  DDRB |= 0b00000001;            /* Data Direction Register B:
                                   writing a one to the bit
                                   enables output. */

  // ------ Event loop ------ //
  while (1) {

    PORTB = 0b00000001;          /* Turn on first LED bit/pin in PORTB */
    _delay_ms(1000);                                           /* wait */

    PORTB = 0b00000000;          /* Turn off all B pins, including LED */
    _delay_ms(1000);                                           /* wait */

  }                                                  /* End event loop */
  return 0;                            /* This line is never reached */
}

```
### 2. [Theory] Questions!!

1. What is avr-gcc and the difference between avr-gcc and gcc
2. Can C standard libary be used in avr-gcc?
3. What is a Cross Compiler? and Is avr-gcc a cross-compiler?
4. What is a flash programmer in the context of embedded system?
5. What is avrdude and its functionalities?
6. What is In-System programming and its PIN? 
7. What is Makefile?
8. Write a code with only "printf("hello world!");" and use makefile to compile this code.
9. what is Toolchain? and how many steps are involed during Toolchain?
10. What is the functionalty of DDRx (Data Direction Register)?
11. What is the functionality of PORTx (Data Register)?

### 3. Programming Exercise!!

#### 跑馬燈

![](https://gitlab.hrz.tu-chemnitz.de/boyue--tu-chemnitz.de/avr_embedded/-/raw/main/task1_Basic_Flash:Compile/image/led_circut.png?ref_type=heads)


### 4. Answers
1. 
  AVR-GCC is specialized for the unique characteristics of AVR microcontrollers. 
  GCC is a general-purpose compiler supporting various programming languages, hardware architecture and operating systems. 
  While it is possible to compile code for AVR microcontrollers using GCC, AVR-GCC is often preferable for these microcontrollers due to its optimization specifically for the AVR architecture. 
  AVR microcontrollers normally face limitations in memory, processing power, and other resources. AVR-GCC addresses these constraints by optimizing the architecture, code size, execution speed, and other factors to efficiently compile code for AVR microcontrollers. Also, it provides specific libraries and headers tailored to AVR microcontrollers, simplifying development for embedded systems and IoT devices.

2. 
  Yes, but typically AVR-GCC use avr-libs as the primary library for AVR microcontrollers.

3. 
  A cross-compiler is a compiler which can not only generate and execute code on the current platform but also produce executable files for other/different platforms.

4. 
  The Flash Programmer is a software used to write data to non-volatile memory (flash memory) from a PC. It also can retrieve data from the flash memory. 
  In order to program the flash memory of a microcontroller, several programming pins connect the microcontroller to an external programmer device. This device is then connected to the PC via a connection port. Data is programmed from the PC and transferred to the external programmer device, which uploads the program to the microcontroller through the flash memory.

5. 
  AVRDUDE is a program which can download and upload the on-chip memories (eg. Flash, EEPROM) of AVR microcontrollers from Atmel. Additionally, it supports the serial programming protocol allowing it to be effectively used via the command line for reading or writing all chip memory types as well as through an interactive mode.  
  AVRDUDE can also support chips not currently included in AVRDUDE’s database. It reads a configuration file at startup, which describes all supported parts and programmers. Therefore, users just need to add the configuration file without waiting for the latest release of AVRDUDE. 

6. 
  In-System Programming (ISP) also known as In-Circuit Serial Programming (ICSP) is a technique/method that enables engineers to program embedded devices, microcontrollers, programmable logic devices, etc., without the need to install them into systems/products in advance. Instead, these devices can be programmed when they are embedded/installed in the system. This technique/method makes production processes much more flexible, saving time and costs.
  There are 6 ICSP pins available on the Arduino Uno board.
  -	PB3: MOSI
  -	PB4: MISO 
  -	PB5: SCK
  -	VCC: 5V
  -	GND: GND
  -	RESET: RESET

7. 
  Makefile contains/records all the dependencies of the source files, programmers don’t need to link all of the dependencies manually among different files.

8. 
  hellomake.h		
  ```
  void printf_makefile(void);	
  ```
  hellomake_func.c
  ```
  #include <stdio.h>
  #include <hellomake.h>

  void printf_makefile(void){
	
	  printf("hello world!\n");
	  return;
  }	
  ```

  hellomake.c
  ```
  #include <hellomake.h>

  int main(){
  	return(0);
  }
  ```

9. 
  A toolchain is a collection of software tools/libraries intending to perform complex software development tasks or to create a software product. It comprises compiler, assembler, linker, libraries (eg. standard C, math, etc.), and a few utilities (eg. debugger, kernel, etc.).
  There are a few steps to chain all components together in Toolchain: <br /><br />
  9-1. build binutils: 
      This step involves building the binutils, which include essential tools like the assembler and linker. These tools are necessary for subsequent steps in the toolchain. <br />
  9-2.	require a bootstrap, initial/core compiler (named it “core pass 1 compiler” here): 
      It’s a simpler compiler that doesn’t need the headers and start files to build the C library start files <br />
  9-3.	add kernel headers: 
      Kernel headers are required for subsequent steps in building the complete C library. <br />
  9-4.	add C library headers and start files: 
      The core pass 2 compiler requires the C library headers and start files to establish the entire C library. These files provide the necessary information for using the C library (to know how to use this C library).  <br />
  9-5.	require a bootstrap, initial/core compiler (named it “core pass 2 compiler” here): 
      It’s a stripped-down compiler capable of building the entire C library but doesn’t require the C library itself. <br />
  9-6.	complete C library: 
      The entire C library is established using the core pass 2 compiler. This step ensures the C library is fully functional and integrated into the toolchain. <br />
  9-7.	final compiler:
      This final compiler is built using the fully established C library and is capable of generating executable code for the target platform.

10. 
  DDRx is a register that can configure the data direction of the port pins (in- or output). When a port pin is set to 1, it means this pin is being set as an output. Conversely, if it is set to 0. then the pin will function as an input. 

11. 
  PORTx is a register that can write data into specific pins (the pins we have set as output in DDRx).


12. 跑馬燈
https://gitlab.hrz.tu-chemnitz.de/ueien-at-tu-chemnitz.de/avr_embedded/-/blob/main/task1_Basic_Flash:Compile/running%20LED.MOV?ref_type=heads

```
#include <avr/io.h>
#include <util/delay.h>

int main(void) {
  DDRB |= 0b00111111;
  DDRD |= 0b11000000;

  while(1){
    PORTB = 0b00000001;
    _delay_ms(100);

    PORTB = 0b00000010;
    _delay_ms(100);

    PORTB = 0b00000100;
    _delay_ms(100);

    PORTB = 0b00001000;
    _delay_ms(100);

    PORTB = 0b00010000;
    _delay_ms(100);

    PORTB = 0b00100000;
    _delay_ms(100);

    PORTB = 0b00000000;
    PORTD = 0b01000000;
    _delay_ms(100);

    PORTD = 0b10000000;
    _delay_ms(100);

    PORTD = 0b00000000;
  }
  return 0;
}

```